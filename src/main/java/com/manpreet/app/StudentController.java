package com.manpreet.app;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/students")
public class StudentController {
	
	@Autowired
	private StudentService studentservice;
	
	
	@RequestMapping(method = RequestMethod.GET)
	public Collection<Student> getAllStudents(){
		return this.studentservice.getAllStudents();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Student> getStudentById(@PathVariable int id) {
		
		Student s = this.studentservice.getStudentById(id);
		if ( s != null) {
			return new ResponseEntity<Student>(s,HttpStatus.OK); 
		} else {
			return new ResponseEntity<Student>(s,HttpStatus.BAD_REQUEST); 
		}
		
	}
	
	
	@RequestMapping(method = RequestMethod.PUT , consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> updateStudent(@RequestBody Student student) {
		if(studentservice.studentExists(student)) {
			studentservice.updateStudent(student);
			return new  ResponseEntity<String>("Successfully Updated Student",HttpStatus.OK); 
		} else {
			return new ResponseEntity<String>("Student Doesnt exist",HttpStatus.BAD_REQUEST);
			
		}
		
	} 
	
	@RequestMapping(method = RequestMethod.POST , consumes = MediaType.APPLICATION_JSON_VALUE)
	public  ResponseEntity<String> addStudent(@RequestBody Student student) {
		if (studentservice.studentExists(student)) {
			return new ResponseEntity<String>("Student Already Exists ",HttpStatus.BAD_REQUEST);	
		} else {
			studentservice.addStudent(student);
			return new ResponseEntity<String>("Student Successfully Added ",HttpStatus.OK);	
			
		}
		
	}
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<String> deleteStudent(@PathVariable int id) {
		
		Student s = this.studentservice.getStudentById(id);
		if ( s != null) {
			studentservice.removeStudentById(id);
			return new ResponseEntity<String>("Student Successfully Removed",HttpStatus.OK); 
		} else {
			return new ResponseEntity<String>("Student Does not Exist",HttpStatus.BAD_REQUEST); 
		}
		
	}
	
 
}
