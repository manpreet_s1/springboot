package com.manpreet.app;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class StudentService {
	
	@Autowired
	private StudentDao studentDao;
	
	public Collection<Student> getAllStudents(){
		return this.studentDao.getAllStudents();
	}
	
	
	public Student getStudentById(int id) {
		return this.studentDao.getStudentById(id);
	}



	public void removeStudentById(int id) {
		this.studentDao.removeStudentById(id);
		
	}
	
	public void updateStudent(Student student) {
		 this.studentDao.updateStudent(student);
		 
		
	}

	public void addStudent(Student student) {
		this.studentDao.addStudent(student);
		
	}
	
	public boolean studentExists(Student student) {
		return studentDao.studentExists(student);
	}

}
 