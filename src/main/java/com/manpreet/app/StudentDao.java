package com.manpreet.app;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;

import org.springframework.stereotype.Repository;

@Repository
public class StudentDao {
	
	private static HashMap<Integer, Student> students = new HashMap<Integer, Student>();
	
	static {
		
		loadStudents();

	}
	
	public static void loadStudents() {
		
		String line = "";
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader("src/main/resources/students.csv"));
			while ((line = reader.readLine()) != null) {
				String[] data = line.split(",");
				students.put(Integer.parseInt(data[0]), new Student(Integer.parseInt(data[0]),data[1],data[2]));
			}
			
		} catch (FileNotFoundException e) {
			System.out.println("file not Found");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.print("IO exception");
			e.printStackTrace();
		} catch (NumberFormatException e) {
			System.out.print("NUmber exception");
			e.printStackTrace();
			
		}
		
	}
	
	public Collection<Student> getAllStudents(){
		return this.students.values();
	}
	
	
	public Student getStudentById(int id) {
		return this.students.get(id);
	}

	public void removeStudentById(int id) {
		this.students.remove(id);
		
	}
	
	public void updateStudent(Student student) {
		 Student s = students.get(student.getId());
		 s.setCourse(student.getCourse());
		 s.setName(student.getName());
		 s.setCourse(student.getCourse());
		 students.put(student.getId(), student);
		 
		
	}

	public void addStudent(Student student) {
		this.students.put(student.getId(), student);
		
		
	}
	
	public boolean studentExists(Student student) {
		if(this.students.containsKey(student.getId()) == true) {
			return true;
		} else {
			return false;
		}
	}
	
	 
	

}
